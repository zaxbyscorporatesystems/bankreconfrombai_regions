﻿using System;
using System.Data;
using System.Windows.Forms;

namespace BankReconfromBAI_Regions
{
    public partial class FrmFixCheckNum : Form
    {
        public DataTable DtDupChecks
        {
            set; get;
        }
        public FrmFixCheckNum()
        {
            InitializeComponent();
        }

        private void FrmFixCheckNum_Load(object sender, EventArgs e)
        {
            dgrdErrors.Rows.Clear();
            foreach (DataRow dtFound in DtDupChecks.Rows)
            {
                dgrdErrors.Rows.Add(dtFound["rowCount"], dtFound["TransNbr"], dtFound["ReconBankAmt"]);
                // new window as modal?????????

            }
            dgrdErrors.Focus();
            dgrdErrors.CurrentCell = dgrdErrors.Rows[0].Cells[1];
            //toolTip1.SetToolTip(dgrdErrors,
            label1.Text = $"Select and correct any of the duplicate check numbers found.{Environment.NewLine}Only check numbers can be corrected.";

            Update();

        }

        private void cmdUpdate_Click(object sender, EventArgs e)
        {
            int cntRow = 0;
            bool bErrors = false;
            DtDupChecks.Clear();
            foreach (DataGridViewRow dgRow in dgrdErrors.Rows)
            {
                string chkNum = (string)dgrdErrors["colCheck", cntRow].Value;
                chkNum = chkNum.PadLeft(10, '0');
                if (chkNum == "0000000000")
                {
                    MessageBox.Show($"Invalid enter; {chkNum}, please correct.", @"CorrectionNeeded", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    bErrors = true;
                    break;
                }
                dgrdErrors["colCheck", cntRow].Value = chkNum;
                DtDupChecks.Rows.Add(dgrdErrors["colRowId", cntRow].Value, dgrdErrors["colCheck", cntRow].Value, dgrdErrors["colAmt", cntRow].Value);
                cntRow++;
            }
            if (!bErrors)
            {
                Close();
            }
            return;
        }
    }
}
