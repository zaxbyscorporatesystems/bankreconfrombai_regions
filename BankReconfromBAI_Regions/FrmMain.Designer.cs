﻿namespace BankReconfromBAI_Regions
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.tlTip = new System.Windows.Forms.ToolTip(this.components);
            this.lblErrs = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.cmdOutputBrowse = new System.Windows.Forms.Button();
            this.tbxOutFile = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdCreate185 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdBrowse = new System.Windows.Forms.Button();
            this.tbxFile = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlTip
            // 
            this.tlTip.AutoPopDelay = 5000;
            this.tlTip.InitialDelay = 100;
            this.tlTip.ReshowDelay = 100;
            this.tlTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // lblErrs
            // 
            this.lblErrs.AutoSize = true;
            this.lblErrs.ForeColor = System.Drawing.Color.Red;
            this.lblErrs.Location = new System.Drawing.Point(84, 165);
            this.lblErrs.Name = "lblErrs";
            this.lblErrs.Size = new System.Drawing.Size(70, 13);
            this.lblErrs.TabIndex = 42;
            this.lblErrs.Text = "Errors Found:";
            this.lblErrs.Visible = false;
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(84, 152);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(41, 13);
            this.lblCount.TabIndex = 41;
            this.lblCount.Text = "Count: ";
            // 
            // cmdOutputBrowse
            // 
            this.cmdOutputBrowse.ForeColor = System.Drawing.Color.Navy;
            this.cmdOutputBrowse.Location = new System.Drawing.Point(660, 104);
            this.cmdOutputBrowse.Name = "cmdOutputBrowse";
            this.cmdOutputBrowse.Size = new System.Drawing.Size(87, 23);
            this.cmdOutputBrowse.TabIndex = 40;
            this.cmdOutputBrowse.Text = "Browse";
            this.cmdOutputBrowse.UseVisualStyleBackColor = true;
            this.cmdOutputBrowse.Click += new System.EventHandler(this.cmdOutputBrowse_Click);
            // 
            // tbxOutFile
            // 
            this.tbxOutFile.Location = new System.Drawing.Point(75, 106);
            this.tbxOutFile.Name = "tbxOutFile";
            this.tbxOutFile.Size = new System.Drawing.Size(579, 20);
            this.tbxOutFile.TabIndex = 39;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(11, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 38;
            this.label1.Text = "Output File";
            // 
            // cmdCreate185
            // 
            this.cmdCreate185.Location = new System.Drawing.Point(660, 141);
            this.cmdCreate185.Name = "cmdCreate185";
            this.cmdCreate185.Size = new System.Drawing.Size(113, 35);
            this.cmdCreate185.TabIndex = 37;
            this.cmdCreate185.Text = "Create CB500 Fiie";
            this.cmdCreate185.UseVisualStyleBackColor = true;
            //this.cmdCreate185.Click += new System.EventHandler(this.cmdCreate185_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuStrip1.Size = new System.Drawing.Size(803, 24);
            this.menuStrip1.TabIndex = 36;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.exitToolStripMenuItem.ForeColor = System.Drawing.Color.Red;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.ForeColor = System.Drawing.Color.Navy;
            this.cmdBrowse.Location = new System.Drawing.Point(660, 64);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(87, 23);
            this.cmdBrowse.TabIndex = 35;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.UseVisualStyleBackColor = true;
            //this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
            // 
            // tbxFile
            // 
            this.tbxFile.Location = new System.Drawing.Point(75, 66);
            this.tbxFile.Name = "tbxFile";
            this.tbxFile.Size = new System.Drawing.Size(579, 20);
            this.tbxFile.TabIndex = 34;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(19, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Input File";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(803, 199);
            this.Controls.Add(this.lblErrs);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.cmdOutputBrowse);
            this.Controls.Add(this.tbxOutFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdCreate185);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.cmdBrowse);
            this.Controls.Add(this.tbxFile);
            this.Controls.Add(this.label2);
            this.ForeColor = System.Drawing.Color.Navy;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Regions: Create CB500 from BAI";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip tlTip;
        private System.Windows.Forms.Label lblErrs;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Button cmdOutputBrowse;
        private System.Windows.Forms.TextBox tbxOutFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdCreate185;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button cmdBrowse;
        private System.Windows.Forms.TextBox tbxFile;
        private System.Windows.Forms.Label label2;
    }
}

