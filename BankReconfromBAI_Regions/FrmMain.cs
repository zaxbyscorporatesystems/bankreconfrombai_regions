﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace BankReconfromBAI_Regions
{
    public partial class FrmMain : Form
    {
        private string _fyleIn = string.Empty,
            _fyleOut = string.Empty,
            _fyleTotals=string.Empty,
            _cashCode ="1355",
            //_destFolder = @"\\amidala\store\Accounting_InFor Solution\Documentation\ZAX LLC Bank Statements\10002 Regions 1355\",
            _bank = "Regions",
            //_i4Acct="10002",
            _fyleCbc = string.Empty,
            //_bankId = @"62000019",
            //_operAcct = @"7887001355",
            //_pmtCode = @"CHK",
            _errSql = @"C:\Infor\Data\BAI Files\errors.txt";
        private int _recAll16Count = 0;
        private decimal _recAll16Amt = 0;

        //_bnkAmtSign=@"+",
        //_status="R";

        private void cmdOutputBrowse_Click(object sender, EventArgs e)
        {
            //SaveFileDialog dlgFyleOut = new SaveFileDialog
            //{
            //    RestoreDirectory = true,
            //    Filter = @"Text files (*.txt)|*.txt"
            //};
            //if (dlgFyleOut.ShowDialog() == DialogResult.OK)
            //{
            //    _fyleOut = dlgFyleOut.FileName.Trim();
            //    tbxOutFile.Text = _fyleOut;
            //}
            //CheckIfOkay();
        }

        public FrmMain()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void connectToDMZ()
        {
            System.Diagnostics.Process objProcess = new System.Diagnostics.Process();
            objProcess.StartInfo.FileName = "connect.bat";
            objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;    // to hide the command window popping up
            objProcess.Start();
            objProcess.WaitForExit();    // Gives time for the process to complete operation.
                                         // After code is executed, call the dispose() method
            objProcess.Dispose();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

            connectToDMZ();
            // testing
            //tbxFile.Text = @"C:\Infor\Data\BAI Files\P3 2016 WF Short Bank Statement.bai2";
            //_fyleIn = tbxFile.Text;
            //tbxOutFile.Text = @"C:\Infor\Data\BAI Files\test.txt";
            //_fyleOut = tbxOutFile.Text;
            //// testing
            //CheckIfOkay();
            //tlTip.SetToolTip(this.tbxFile,
            //    @"Enter or select via the Browse button the path and name of the bank file (*.bai) to be loaded.");
            //tlTip.SetToolTip(this.tbxOutFile,
            //    @"Enter or select via the Browse button the path and name of the file to be created for loading purposes.");
            //tlTip.SetToolTip(this.cmdCreate185,
            //    @"Click here to read the selected input file and create the defined output file.");

            //}

            //private void cmdBrowse_Click(object sender, EventArgs e)
            //{
            //OpenFileDialog dlgFyleIn = new OpenFileDialog
            //{
            //    RestoreDirectory = true,
            //    Filter = @"All files (*.*)|*.*"
            //};
            //if (dlgFyleIn.ShowDialog() == DialogResult.OK)
            //{
            //    _fyleIn = dlgFyleIn.FileName.Trim();
            //    tbxFile.Text = _fyleIn;
            //}
            //CheckIfOkay();
            //}

            // private void CheckIfOkay()
            // {
            //            cmdCreate185.Enabled = (tbxFile.Text.Trim().Length > 0 && tbxOutFile.Text.Trim().Length > 0) ? true : false;
            // cmdCreate185.Enabled = tbxFile.Text.Trim().Length > 0 ? true : false;
            //}

            //private void cmdCreate185_Click(object sender, EventArgs e)
            //{
            // time to work
            //Cursor = Cursors.WaitCursor;
            _errSql = $@"C:\Infor\Data\BAI Files\errors_{_cashCode}_{DateTime.Now.Month}_{DateTime.Now.Day}.txt";

            bool curTrans = false;
            StringBuilder sbTransLyne = new StringBuilder(),
                sbSql = new StringBuilder();
            int count = 0,
                countInFyle = 0;
            string issueDate = string.Empty;
            string[] fylesToLoad = Directory.GetFiles(Properties.Settings.Default.ftpPath);
            foreach (string fyle in fylesToLoad)
            {
                _fyleIn = fyle;
                FileInfo fInfo = new FileInfo(_fyleIn);
                DateTime dtFyleCreate = fInfo.CreationTime;
                //_fyleCbc = $@"C:\Infor\Data\BAI Files\CB500_{_cashCode}_{_bank}_{dtFyleCreate.Month}_{dtFyleCreate.Day}.csv";
                _fyleTotals = $@"C:\Infor\Data\BAI Files\BAI_Totals_{_cashCode}_{_bank}_{dtFyleCreate.Month}_{dtFyleCreate.Day}.txt";
                string reconDte = $"{DateTime.Now.Year}{DateTime.Now.Month.ToString().Trim().PadLeft(2, '0')}{DateTime.Now.Day.ToString().Trim().PadLeft(2, '0')}";
                try
                {
                    if (File.Exists(_fyleCbc))
                    {
                        File.Delete(_fyleCbc);
                    }
                    if (File.Exists(_errSql))
                    {
                        File.Delete(_errSql);
                    }
                    if (File.Exists(_fyleOut))
                    {
                        File.Delete(_fyleOut);
                    }
                    if (File.Exists(_fyleTotals))
                    {
                        File.Delete(_fyleTotals);
                    }
                    //using (CodeSqlCe clsSqlCe = new CodeSqlCe())
                    using (CodeSQL clsSql = new CodeSQL())
                    {
                        clsSql.CmdNonQuery(Properties.Settings.Default.aConCMS, "DELETE FROM [BaiData]");
                       //clsSqlCe.ExecuteNonQuery("DELETE FROM [BaiData]");

                        string[] inLynes = File.ReadAllLines(_fyleIn);
                        long curLyneCnt = 0;

                        while (curLyneCnt < inLynes.GetLength(0))
                        {
                            string[] fields = inLynes[curLyneCnt].Split(','); //lyne.Split(',');
                            switch (fields[0])
                            {
                                case "02":
                                    // on 8/2/2016 Marc said to not back up one day
                                    //DateTime dteTemp = Convert.ToDateTime($"{fields[4].Substring(2, 2)}/{fields[4].Substring(4)}/20{fields[4].Substring(0, 2)}");
                                    //dteTemp = dteTemp.AddDays(-1);
                                    issueDate = $"20{fields[4].Substring(0, 2)}{fields[4].Substring(2, 2)}{fields[4].Substring(4)}";
                                    //issueDate = $"{dteTemp.Year}{dteTemp.Month:00}{dteTemp.Day:00}";
                                    _fyleCbc = $@"C:\Infor\Data\BAI Files\CB500_{_cashCode}_{_bank}_{fields[4].Substring(2, 2)}_{fields[4].Substring(4)}.csv";      //per marc 7/26/2016
                                    break;
                                case "16":
                                    //start new trans
                                    _recAll16Count++;
                                    sbTransLyne.Clear();
                                    string chkNumber = string.Empty,
                                        lyne88 = string.Empty;
                                    //if (fields[1] == "175")
                                    //{
                                    //lblCount.Text = $"Transaction Count: {count}";
                                    //lblCount.Update();
                                    curTrans = true;
                                    lyne88 = string.Empty;
                                    while (curTrans)
                                    {
                                        if (inLynes[curLyneCnt + 1].StartsWith("88,"))
                                        {
                                            curLyneCnt++;
                                            lyne88 += inLynes[curLyneCnt].Trim();
                                            lyne88 = lyne88.Replace("'", "");
                                        }
                                        else
                                        {
                                            curTrans = false;
                                        }

                                    }
                                    //if (fields[3].Trim().ToUpper() == "V" || fields[3].Trim().ToUpper() == "S")
                                    //{
                                    chkNumber = fields[6].Trim();
                                    //}
                                    //else
                                    //{
                                    //    chkNumber = fields[5].Trim();
                                    //}
                                    chkNumber = chkNumber.Length < 10 ? chkNumber.PadLeft(10, '0') : chkNumber;
                                    while (chkNumber.Length > 10)
                                    {
                                        chkNumber = chkNumber.Substring(1);
                                    }
                                    sbSql.Clear();
                                    if (fields[1].Trim() == "698")
                                    {
                                        fields[6] = "SVC CHG";
                                    }

                                    if (fields[6].Length > 10)
                                    {
                                        fields[6] = fields[6].Substring(0, 10); ///AMB  5/15/2019  got error when "DEBIT ADJUSTMENT" so trimming to field length
                                    }
                                    count++;
                                    sbSql.Append("INSERT INTO[BaiData] ([rowCount],[BaiNbr],[TransNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88]) VALUES(");
                                    sbSql.Append($"{count}");
                                    sbSql.Append($",'{fields[1]}'");
                                    sbSql.Append($",'{fields[6].ToString().Replace("/", "")}'");
                                    sbSql.Append($",'{fields[2]}'");
                                    sbSql.Append($",'{reconDte}'");
                                    sbSql.Append($",'{issueDate}'");
                                    sbSql.Append($",'{fields[4]}'");
                                    sbSql.Append(")");
                                    //if (clsSqlCe.ExecuteNonQuery(sbSql.ToString()) != 1)
                                    if (clsSql.CmdNonQuery(Properties.Settings.Default.aConCMS, sbSql.ToString()) != 1)
                                    {
                                        File.AppendAllText(_errSql, sbSql.ToString() + Environment.NewLine);
                                    }
                                    curTrans = false;
                                    //}
                                    break;
                                case "88":
                                    break;
                                default:
                                    break;
                            }
                            curLyneCnt++;
                        }
                        // check for dups & "0000000000" check numbers
                        GC.Collect();
                        // file totals here
                        string tmpBAI;
                        string cnt16 = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, "select count(*) from [BaiData]").ToString();
                        string dollars16 = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, "select sum(cast([ReconBankAmt] as decimal (18,4))) from [BaiData]").ToString();
                        //string cnt16 = clsSqlCe.ExecuteScalar("select count(*) from [BaiData]");
                        //string dollars16 = clsSqlCe.ExecuteScalar("select sum(cast([ReconBankAmt] as decimal (18,4))) from [BaiData]");
                        decimal sumAmt = decimal.TryParse(dollars16, out sumAmt) ? sumAmt : 0;
                        sumAmt = Math.Round(sumAmt / 100, 2);
                        _recAll16Count = int.TryParse(cnt16, out countInFyle) ? countInFyle : 0;
                        _recAll16Amt = sumAmt;
                        File.AppendAllText(_fyleTotals, $@"Issue Date: {issueDate.Substring(4, 2)}/{issueDate.Substring(6)}/{issueDate.Substring(0, 4)}    BAI_Totals{Environment.NewLine}");
                        File.AppendAllText(_fyleTotals, Environment.NewLine);
                        File.AppendAllText(_fyleTotals, $"Number of 16 records: {cnt16} for a total amount of {sumAmt:C}" + Environment.NewLine);
                        DataTable dtBai = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, "select distinct([BaiNbr]) from [BaiData]").Tables[0];
                        //DataTable dtBai = clsSqlCe.GetDataset("select distinct([BaiNbr]) from [BaiData]");
                        foreach (DataRow baiR in dtBai.Rows)
                        {
                            tmpBAI = baiR["BaiNbr"].ToString().Trim();
                            cnt16 = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select count(*) from [BaiData] where [BaiNbr]='{tmpBAI}'").ToString();
                            dollars16 = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select sum(cast([ReconBankAmt] as decimal (18,4))) from [BaiData] where [BaiNbr]='{tmpBAI}'").ToString();
                            //cnt16 = clsSqlCe.ExecuteScalar($"select count(*) from [BaiData] where [BaiNbr]='{tmpBAI}'");
                            //dollars16 = clsSqlCe.ExecuteScalar($"select sum(cast([ReconBankAmt] as decimal (18,4))) from [BaiData] where [BaiNbr]='{tmpBAI}'");
                            sumAmt = decimal.TryParse(dollars16, out sumAmt) ? sumAmt : 0;
                            sumAmt = Math.Round(sumAmt / 100, 2);
                            File.AppendAllText(_fyleTotals, $"BAI {tmpBAI} records: {cnt16} for a total amount of {sumAmt:C}{Environment.NewLine}");
                        }
                        // end totals

                        // time to create the file
                        GC.Collect();
                        //lblErrs.ForeColor = Color.Navy;
                        //lblErrs.Text = @"Building CB500 input file.";
                        //lblErrs.Visible = true;
                        string paymentCode,
                            tmp88,
                            tmp,
                            amt;
                        int seqNum = 0;
                        decimal sglAmt = 0,
                            sumAmt500 = 0;
                        DataTable dtToWrite = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, "Select [rowCount],[BaiNbr],[TransNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88] from [BaiData];").Tables[0];
                        //DataTable dtToWrite = clsSqlCe.GetDataset("Select [rowCount],[BaiNbr],[TransNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88] from [BaiData];");
                        // File.AppendAllText(_fyleCbc,@"CVC-RUN-GROUP,CVC-CASH-CODE,CVC-BANK-INST-CODE,CVC-TRANS-NBR,CVC-SEQ-NBR,CVC-COMPANY,CVC-VENDOR,CVC-REC-STATUS,CVC-SOURCE-CODE,CVC-ISSUE-DATE,CVC-ISSUE-BNK-AMT,CVC-ISSUE-BASE-AMT,CVC-RECON-DATE,CVC-RECON-BNK-AMT,CVC-RECON-BASE-AMT,CVC-DESCRIPTION,CVC-REFERENCE,CVC-DIS-ACCT-UNIT,CVC-DIS-ACCOUNT,CVC-DIS-SUB-ACCT,CVC-TAX-CODE,CVC-TRAN-TAXABLE,CVC-TRAN-TAX-AMT,CVC-JRNL-BOOK-NBR,CVC-ISSUE-TRAN-AMT,CVC-BNK-CNV-RATE,CVC-CURRENCY-CODE,CVC-BANK-ND,CVC-TRAN-ND,CVC-STMT-STATUS,CVC-PAY-GROUP,CVC-ORIG-CNV-RATE,CVC-SEGMENT-BLOCK,CVC-SOURCE,CVC-ACTIVITY,CVC-ACCT-CATEGORY,CVC-ANALYSIS-FLD,CVC-USER-FIELD1,CVC-USER-FIELD2,CVC-USER-FIELD3,CVC-USER-FIELD4,CVC-POST-DATE,CVC-DIS-COMPANY,CVC-DIS-SEG-BLOCK" +Environment.NewLine);
                        using (CodeGetTransactionDetails clsGetit = new CodeGetTransactionDetails())
                        {
                            foreach (DataRow drWrite in dtToWrite.Rows)
                            {
                                amt = drWrite["ReconBankAmt"].ToString().TrimStart('0');
                                sglAmt = decimal.TryParse(amt, out sglAmt) ? sglAmt : 0;
                                sglAmt = Math.Round(sglAmt / 100, 2);
                                sumAmt500 += sglAmt;
                                amt = string.Format("{0:0.00}", sglAmt);

                                paymentCode = drWrite["BaiNbr"].ToString().Trim(); // == "475" ? _pmtCode : "TBD";
                                sbTransLyne.Clear();
                                string chkNumb = drWrite["TransNbr"].ToString().Trim();
                                chkNumb = chkNumb.TrimStart('0');
                                chkNumb = chkNumb.Length < 10 ? chkNumb.PadLeft(10, '0') : chkNumb;
                                while (chkNumb.Length > 10)
                                {
                                    chkNumb = chkNumb.Substring(1);
                                }
                                // cbctrans csv
                                seqNum++;
                                tmp88 = drWrite["Data88"].ToString();
                                //tmp88 = tmp88.StartsWith("88,") ? tmp88.Substring(3) : tmp88;
                                //tmp88 = tmp88.Replace("88,", "|");
                                //string[] split88 = tmp88.Split('|');
                                sbTransLyne.Clear();
                                sbTransLyne.Append(
                                    $"{DateTime.Now.Year}{DateTime.Now.Month.ToString().PadLeft(2, '0')}{DateTime.Now.Day.ToString().PadLeft(2, '0')}{_cashCode}");
                                //CVC - RUN - GROUP
                                sbTransLyne.Append($",{_cashCode}"); //CVC - CASH - CODE
                                sbTransLyne.Append($",{drWrite["BaiNbr"]}"); //CVC - BANK - INST - CODE
                                                                             // per marc 7/29/2016 include
                                                                             //if (drWrite["BaiNbr"].ToString().Trim() == "455")
                                                                             //{
                                                                             //    tmp = "ERROR DO NOT PROCESS";
                                                                             //    break;
                                                                             //}
                                                                             //else
                                                                             //{
                                                                             //tmp = dtFyleCreate.Year.ToString().Substring(2);
                                                                             //tmp = $"{dtFyleCreate.Year.ToString().Substring(2)}{dtFyleCreate.Month.ToString().PadLeft(2, '0')}{dtFyleCreate.Day.ToString().PadLeft(2, '0')}{seqNum.ToString().PadLeft(4, '0')}";
                                tmp = $"{issueDate.Substring(2)}{seqNum.ToString().PadLeft(4, '0')}";        //per Marc 8/1/2016
                                //}

                                sbTransLyne.Append($",{tmp}"); //CVC - TRANS - NBR
                                sbTransLyne.Append($",{seqNum}"); //CVC - SEQ - NBR
                                sbTransLyne.Append(",200"); //CVC - COMPANY
                                sbTransLyne.Append(","); //CVC - VENDOR
                                sbTransLyne.Append(",2"); //CVC - REC - STATUS
                                sbTransLyne.Append(",99"); //CVC - SOURCE - CODE
                                sbTransLyne.Append($",{drWrite["IssueDate"]}"); //CVC - ISSUE - DATE
                                sbTransLyne.Append($",{amt}"); //CVC - ISSUE - BNK - AMT
                                sbTransLyne.Append($",{amt}"); //CVC - ISSUE - BASE - AMT
                                sbTransLyne.Append($",{drWrite["IssueDate"]}"); //CVC - RECON - DATE
                                sbTransLyne.Append($",{amt}"); //CVC - RECON - BNK - AMT
                                sbTransLyne.Append($",{amt}"); //CVC-RECON-BASE-AMT
                                sbTransLyne.Append($",{drWrite["TransNbr"].ToString().Trim()}");
                                //CVC - DESCRIPTION
                                sbTransLyne.Append(","); //CVC-REFERENCE

                                clsGetit.Rec88 = drWrite["Data88"].ToString().Trim().TrimStart('0');
                                clsGetit.BaiCode = drWrite["BaiNbr"].ToString().Trim();
                                clsGetit.UnScramble88();
                                sbTransLyne.Append($",{clsGetit.AcctUnit}"); //CVC - DIS - ACCT - UNIT
                                sbTransLyne.Append($",{clsGetit.DistAcct}"); //CVC - DIS - ACCOUNT
                                sbTransLyne.Append($",{clsGetit.SubAcct}"); //CVC - sub - ACCOUNT
                                sbTransLyne.Append(","); //CVC-TAX-CODE,
                                sbTransLyne.Append(","); //CVC-TRAN-TAXABLE
                                sbTransLyne.Append(","); // CVC - TRAN - TAX - AMT
                                sbTransLyne.Append(","); // CVC-JRNL-BOOK-NBR
                                sbTransLyne.Append($",{amt}"); //CVC - ISSUE - TRAN - AMT
                                sbTransLyne.Append(","); // CVC-BNK-CNV-RATE
                                sbTransLyne.Append(","); // CVC-CURRENCY-CODE
                                sbTransLyne.Append(","); // CVC-BANK-ND
                                sbTransLyne.Append(","); // CVC-TRAN-ND
                                sbTransLyne.Append(","); // CVC-STMT-STATUS
                                sbTransLyne.Append(","); //CVC-PAY-GROUP
                                sbTransLyne.Append(","); // CVC-ORIG-CNV-RATE
                                sbTransLyne.Append(","); // CVC-SEGMENT-BLOCK
                                sbTransLyne.Append(","); // CVC-SOURCE
                                sbTransLyne.Append(","); // CVC-ACTIVITY
                                sbTransLyne.Append(","); //  CVC-ACCT-CATEGORY
                                sbTransLyne.Append(","); // CVC-ANALYSIS-FLD
                                sbTransLyne.Append(","); // CVC-USER-FIELD1
                                sbTransLyne.Append(","); // CVC-USER-FIELD2
                                sbTransLyne.Append(","); // CVC-USER-FIELD3
                                sbTransLyne.Append(","); // CVC-USER-FIELD4
                                sbTransLyne.Append(","); // CVC-POST-DATE
                                sbTransLyne.Append(","); // CVC-DIS-COMPANY
                                sbTransLyne.Append(","); // CVC-DIS-SEG-BLOCK

                                File.AppendAllText(_fyleCbc, sbTransLyne.ToString() + Environment.NewLine);
                                clsSql.CmdNonQuery(Properties.Settings.Default.aConCMS, $"delete from [BaiData] where [rowCount]={drWrite["rowCount"]}");
                                //clsSqlCe.ExecuteNonQuery($"delete from [BaiData] where [rowCount]={drWrite["rowCount"]}");

                            }
                            File.AppendAllText(_fyleTotals, Environment.NewLine);
                            File.AppendAllText(_fyleTotals, $"CB500 16 records: {seqNum} for a total amount of {sumAmt500:C}" + Environment.NewLine);
                            File.AppendAllText(_fyleTotals, $"Not in CB500 records: {_recAll16Count - seqNum} for a total amount of {_recAll16Amt - sumAmt500:C}" + Environment.NewLine);
                            DataSet notLoaded = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, "select [BaiNbr],[TransNbr],[ReconBankAmt],[ReconDate],[IssueDate],[Data88] from [BaiData] order by[BaiNbr]");
                            //if (notLoaded!= null && notLoaded.Rows.Count > 0)
                            if (notLoaded.Tables.Count > 0 && notLoaded.Tables[0].Rows.Count > 0)
                            {
                                File.AppendAllText(_fyleTotals, $"[BaiNbr]|[TransNbr]|[ReconBankAmt]|[ReconDate]|[IssueDate]|[Data88]{Environment.NewLine}");
                                //foreach (DataRow bdRow in notLoaded.Rows)
                                foreach (DataRow bdRow in notLoaded.Tables[0].Rows)
                                {
                                    File.AppendAllText(_fyleTotals, $"{bdRow["BaiNbr"]}|{bdRow["TransNbr"]}|{bdRow["ReconBankAmt"]}|{bdRow["ReconDate"]}|{bdRow["IssueDate"]}|{bdRow["Data88"]}{Environment.NewLine}");
                                }
                            }
                        }
                        curTrans = false;
                        GC.Collect();
                        //lblErrs.BackColor = Color.GhostWhite;
                        //lblErrs.Text = @"CB500 file complete.";
                        try
                        {
                            string fyleInCopy = checkExistingFyle(Properties.Settings.Default.pthSave + Path.GetFileName(_fyleIn));
                            File.Copy(_fyleIn, fyleInCopy, true);
                            string fyleCbcCopy = checkExistingFyle(Properties.Settings.Default.pthCB500 + Path.GetFileName(_fyleCbc));
                            File.Copy(_fyleCbc, fyleCbcCopy, true);
                            string fyleCB500toProd = Properties.Settings.Default.pthCB500Prod + $"CB500_{_cashCode}.csv";
                            File.Copy(_fyleCbc, fyleCB500toProd, true);

                            File.Delete(_fyleIn);
                            File.Delete(_fyleCbc);
                            // send email
                            using (CodeEmail clsEmail = new CodeEmail())
                            {
                                StringBuilder sbMsg = new StringBuilder();
                                sbMsg.AppendLine();
                                sbMsg.AppendLine();
                                string[] totalsFyle = File.ReadAllLines(_fyleTotals);
                                foreach (string mLyne in totalsFyle)
                                {
                                    if (mLyne.Trim().Length > 0)
                                    {
                                        sbMsg.AppendLine($"{mLyne}{Environment.NewLine}");
                                        // sbMsg.AppendLine();
                                    }
                                }
                                if (File.Exists(_errSql))
                                {
                                    string[] errors = File.ReadAllLines(_errSql);
                                    sbMsg.AppendLine();
                                    sbMsg.AppendLine("Errors from Bank BAI file.");
                                    sbMsg.AppendLine();
                                    foreach (string eLyne in errors)
                                    {
                                        sbMsg.AppendLine(eLyne);
                                        sbMsg.AppendLine();
                                    }
                                }
                                clsEmail.ExtMsg = sbMsg.ToString();
                                clsEmail.Bank = $"{_bank}({_cashCode})";
                                clsEmail.InFyle = Path.GetFileName(fyleInCopy);
                                clsEmail.OutFyle = $"{Path.GetFileName(fyleCbcCopy)}";
                                clsEmail.SendEmail();
                            }

                        }
                        catch (Exception ex)
                        {
                            File.AppendAllText(_errSql, @"File copy error: " + ex.Message + Environment.NewLine);
                        }
                    }
                }
                catch (Exception ex)
                {
                    File.AppendAllText(_errSql, ex.Message + Environment.NewLine);
                    //Cursor = Cursors.Default;
                    return;
                }
            }
            //Cursor = Cursors.Default;
            try
            {
                deleteOldFiles(Properties.Settings.Default.pthCB500, 90, "csv");
                deleteOldFiles(Properties.Settings.Default.pthSave, 90, "txt");
            }
            catch
            { }
            Application.Exit();
        }
        private void deleteOldFiles(string folDer, int daysOld, string fyleExt)
        {
            try
            {
                //C:\inetpub\logs\LogFiles\W3SVC1  on naboo and balmorra
                DateTime today = DateTime.Now;
                DirectoryInfo drInfo = new DirectoryInfo(folDer);
                foreach (var dyrectory in drInfo.GetDirectories())
                {
                    string[] fyles = Directory.GetFiles(String.Format(@"{0}", folDer), $"*.{fyleExt}");
                    foreach (var fyle in fyles)
                    {
                        FileInfo fylInfo = new FileInfo(fyle);    //String.Format(@"{0}\{1}\{2}", dyrLog, dyrectory, fyle));
                        if ((today - fylInfo.CreationTime).Days > daysOld)                          //45 days for naboo and balmorra
                        {
                            File.Delete(fyle);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return;
        }

        private string checkExistingFyle(string fyleToCheck)
        {
            string rslt = fyleToCheck;
            int cnt = 0;
            while (File.Exists(rslt))
            {
                cnt++;
                rslt = Path.GetDirectoryName(fyleToCheck) + $@"\V_{cnt}_" + Path.GetFileName(fyleToCheck);
            }

            return rslt;
        }

        private string PopulateDescription(string code)
        {
            string rslt = string.Empty;
            switch (code)
            {
                case "175":
                    rslt = "DEPOSIT CREDIT";
                    break;
                case "455":
                    rslt = "POST TO ERROR SUSPENSE";
                    break;
                case "698":
                    rslt = "BANK FEES DEBIT";
                    break;
                case "699":
                    rslt = "FUNDING FOR NEW STORE TILLS DEBIT";
                    break;
                default:
                    break;
            }
            return rslt;
        }
    }
}
