﻿using System;
using System.Data.SqlServerCe;
using System.Data;

namespace BankReconfromBAI_Regions
{
    class CodeSqlCe : IDisposable
    {

        public DateTime MinDte = Convert.ToDateTime("01/01/1900");

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public string ErrorMessage { set; get; }

        /// <summary>Gets the path of the application folder.</summary>
        /// <returns>String of the path.</returns>
        public string GetDataPath()
        {
            string curDrive = string.Empty;
            curDrive = System.Environment.CurrentDirectory;
            return curDrive;
        }

        /// <summary>Get the connection data source for the client database.</summary>
        /// <returns>A string data type of the data source</returns>
        public string SqlceClientFile()
        {
            string rslt = string.Empty;
            string fldr = Properties.Settings.Default.folderIn;
            rslt = string.Format(@"Data Source={0}\BaiTemp.sdf;Mode=Shared Read", fldr);
            return rslt;
        }

        /// <summary>This method can be used to save or update a database table based on the query string parameter. It will also lock the table until the query is complete.</summary>
        /// <param name="sLQury">The SQL query string to be executed.</param>
        /// <returns>The number of data rows updated or added. If a -1 is returned there was an error in the transaction query.</returns>
        public int ExecuteNonQuery(string sLQury)
        {
            ErrorMessage = string.Empty;
            int rowsUpdated = 0;
            using (SqlCeConnection cnn = new SqlCeConnection(SqlceClientFile()))
            {
                cnn.Open();
                SqlCeCommand sSqlCECmd = new SqlCeCommand();
                try
                {
                    sSqlCECmd.Connection = cnn;
                    sSqlCECmd.CommandText = sLQury;
                    rowsUpdated = sSqlCECmd.ExecuteNonQuery();
                }
                catch (SqlCeException ex)
                {
                    ErrorMessage = ex.Message;
                    // RecordSQLError("SQL Error", "ExecuteNonQuery", sLQury, ex);
                }
                try
                {
                    cnn.Close();
                }
                catch
                {
                }
            }
            return rowsUpdated;
        }
        /// <summary>Checks to see if an existing data item exists.</summary>
        /// <param name="sLQury">The SQL query to test for the data existance.</param>
        /// <returns>True is the data item exists, otherwise false.</returns>
        public bool DataRowExists(string sLQury)
        {
            bool rslt = true;
            DataTable dt = GetDataset(sLQury);
            if (dt == null || dt.Rows.Count == 0)
            {
                rslt = false;
            }
            return rslt;
        }

        /// <summary>This method will return a dataset populated by the query string parameter from the client database.</summary>
        /// <param name="sql">The SQL query string to be executed.</param>
        /// <returns>The datatable result from the query or null if an error is encountered.</returns>
        public DataTable GetDataset(string sql)
        {
            ErrorMessage = string.Empty;
            DataTable fndRows = new DataTable();
            using (SqlCeConnection cnn = new SqlCeConnection(SqlceClientFile()))
            {
                cnn.Open();
                SqlCeCommand sSqlCECmd = new SqlCeCommand();
                sSqlCECmd.Connection = cnn;
                sSqlCECmd.CommandText = sql;
                SqlCeDataAdapter rwsFound = new SqlCeDataAdapter();
                rwsFound.SelectCommand = sSqlCECmd;
                try
                {
                    rwsFound.Fill(fndRows);
                }
                catch (SqlCeException ex)
                {
                    ErrorMessage = ex.Message;
                    fndRows = null;
                    //RecordSQLError("SQL Error", "ExecuteNonQuery", sql, ex);
                }
            }
            return fndRows;
        }

        /// <summary>Executes a scalar query on the client database.</summary>
        /// <param name="sql">The string data type of the SQL query to execute.</param>
        /// <returns>The first column of the first row in the result set is returned.</returns>
        public string ExecuteScalar(string sql)
        {
            ErrorMessage = string.Empty;
            object value = null;
            using (SqlCeConnection cnn = new SqlCeConnection(SqlceClientFile()))
            {
                try
                {
                    cnn.Open();
                    SqlCeCommand sSqlCECmd = new SqlCeCommand();
                    sSqlCECmd.Connection = cnn;
                    sSqlCECmd.CommandText = sql;
                    value = sSqlCECmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    ErrorMessage = ex.Message;
                    value = null;
                    return "Error:" + ex.Message;
                }
                if (value != null)
                {
                    return value.ToString();
                }
                return string.Empty;
            }
        }
        #region date functions--------------------------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDte"></param>
        /// <returns></returns>
        public string ObjDateToStr(object objDte)
        {
            string rtnDte = MinDte.ToShortDateString(),     //"00/00/0000",
                tmpDte = objDte.ToString();
            DateTime tmp;
            try
            {
                tmp = Convert.ToDateTime(objDte);
                //rtnDte = string.Format("{0:00}/{1:00}/{2:0000}", tmp.Month, tmp.Day, tmp.Year);
                //return rtnDte;
            }
            catch
            {
                tmp = Convert.ToDateTime(MinDte);
            }
            if (tmp.Year < 1900)
            {
                tmp = Convert.ToDateTime(MinDte);
            }
            rtnDte = string.Format("{0:00}/{1:00}/{2:0000}", tmp.Month, tmp.Day, tmp.Year);
            //if (!(tmpDte.Contains("/") || tmpDte.Contains("-")))
            //{
            //    tmpDte = string.Format("{0}/{1}/{2}", tmpDte.Substring(0, 2), tmpDte.Substring(2, 2), tmpDte.Substring(4));
            //}
            if (tmpDte == MinDte.ToString() || tmpDte == MinDte.ToShortDateString())
            {
                rtnDte = MinDte.ToShortDateString();
            }
            return rtnDte;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDte"></param>
        /// <returns></returns>
        public DateTime ObjToDateTime(object objDte)
        {
            DateTime rtnDte = DateTime.Now;
            string tmpDte = string.Empty;
            if (Object.ReferenceEquals(objDte.GetType(), rtnDte.GetType()))
            {
                rtnDte = Convert.ToDateTime(objDte);
            }
            else
            {
                tmpDte = objDte.ToString();
                if (tmpDte.Trim().Length > 0)
                {
                    if (!(tmpDte.Contains("/") || tmpDte.Contains("-")))
                    {
                        tmpDte = string.Format("{0}/{1}/{2}", tmpDte.Substring(0, 2), tmpDte.Substring(2, 2), tmpDte.Substring(4));
                    }
                    rtnDte = Convert.ToDateTime(tmpDte);
                }
            }
            return rtnDte;
        }
        /// <summary>Converts a DateTime data type into mm/dd/yyyy string.</summary>
        /// <param name="sDte">A DataTime data type.</param>
        /// <returns>A string in the form of mm/dd/yyyy or 00/00/0000 if the DateTime.MinValue is the input value.</returns>
        public string DateTimeToMDY(object oDte)
        {
            DateTime sDte = MinDte;
            string rslt = "00/00/0000";
            if (oDte == null || oDte.ToString().Trim().Length == 0)
            {
                return rslt;
            }
            if (oDte.GetType() == typeof(DateTime))
            {
                try
                {
                    sDte = Convert.ToDateTime(oDte);
                    rslt = string.Format("{0:00}/{1:00}/{2}", sDte.Month, sDte.Day, sDte.Year);
                }
                catch
                {
                    sDte = MinDte;
                }
            }
            if (sDte == MinDte)
            {
                rslt = "00/00/0000";
            }
            return rslt;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CheckInt(object obj)
        {
            int rslt = 0;
            try
            {
                rslt = Convert.ToInt32(obj);
            }
            catch
            {
                rslt = 0;
            }
            return rslt;
        }
        /// <summary>Check string for the existance of (') and convert to ('') to make SQL happy.</summary>
        /// <param name="obj">Input value, usually a string.</param>
        /// <returns>Converted value to string.</returns>
        public string CheckStr(object obj)
        {
            string rslt = string.Empty;
            if (obj != null && obj.ToString().Trim().Length != 0)
            {
                rslt = string.Format("{0}", obj);
                rslt = rslt.Replace("'", "''");  //replace single ' with double ' to make sql happy
            }
            return rslt.Trim();
        }
        /// <summary>Check the object for null value.</summary>
        /// <param name="obj">The object data type to evaluate.</param>
        /// <returns>The double value of the object or zero if null.</returns>
        public double CheckDouble(object obj)
        {
            double rslt = 0;
            if (obj != null && obj.ToString().Trim().Length != 0)
            {
                rslt = Convert.ToDouble(obj);
            }
            return rslt;
        }
        /// <summary>Check the object for null value.</summary>
        /// <param name="obj">The object data type to evaluate.</param>
        /// <returns>The decimal value of the object or zero if null.</returns>
        public decimal CheckDecimal(object obj)
        {
            decimal rslt = 0;
            if (obj != null && obj.ToString().Trim().Length != 0)
            {
                string tmp = string.Format("{0}", obj);
                tmp = tmp.Replace("$", "");
                tmp = tmp.Replace(",", "");
                tmp = tmp.Replace("*", "");
                rslt = Convert.ToDecimal(tmp);
            }
            return rslt;
        }
        public string CheckForMinDate(object sdte)
        {
            string rslt = string.Empty;
            if (sdte != null)
            {
                if (StrDateToDateTime(sdte) != MinDte)
                {
                    rslt = StrDateToDateTime(sdte).ToShortDateString();
                }
            }
            return rslt;
        }
        public bool CheckBool(object val)
        {
            bool rslt = false;
            if (val != null)
            {
                try
                {
                    rslt = Convert.ToBoolean(val);
                }
                catch
                {
                    rslt = false;
                }
            }
            return rslt;
        }
        /// <summary>Converts a string data type in the form of mm/dd/yyyy to a DateTime data type.</summary>
        /// <param name="sdte">The date as a string in the format of mm/dd/yyyy.</param>
        /// <returns>The string value as a DateTime data type. If the input is null, empty or 00/00/0000 the DateTime.MinValue is returned.</returns>
        public DateTime StrDateToDateTime(object sdte)
        {
            DateTime rslt = MinDte;
            if (sdte == null || sdte.ToString().Trim().Length == 0 || sdte.ToString().Trim() == "00/00/0000")
            {
                return rslt;
            }
            try
            {
                rslt = Convert.ToDateTime(sdte);
            }
            catch
            {
                //some error so leave as min value
            }
            return rslt;
        }
        public string RepairClientFile()
        {
            string rslt = string.Empty;
            SqlCeEngine engine = new SqlCeEngine(SqlceClientFile());
            try
            {
                engine.Repair(null, RepairOption.RecoverAllOrFail);
            }
            catch (Exception ex)
            {
                rslt = ex.Message;
            }
            return rslt;
        }

    }
}
