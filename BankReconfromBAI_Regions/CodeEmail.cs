﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Data;

namespace BankReconfromBAI_Regions
{
    class CodeEmail : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public string Bank
        { set; get; }
        public string InFyle
        { set; get; }
        public string OutFyle
        { set; get; }
        public string ExtMsg
        { set; get; }

        /// <summary>Send the email message.</summary>
        /// <returns>True if sent, false if sror encountered.</returns>
        public bool SendEmail()
        {
            bool rslt = true;
            string recEmailFyle = string.Empty;
            try
            {

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.Subject = $"CB loads for {Bank}";
                StringBuilder sbMessage = new StringBuilder();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("email.zaxbys.com");
                StringBuilder sbRecEmail = new StringBuilder();
                using (CodeSQL clsSql = new CodeSQL())
                {
                    DataSet dsEmails = clsSql.CmdDataset(Properties.Settings.Default.aConCMS, "select [email] from [emaillist]");
                    if (dsEmails.Tables.Count > 0 && dsEmails.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow drEmail in dsEmails.Tables[0].Rows)
                        {
                            message.To.Add(drEmail["email"].ToString().Trim());
                        }
                    }
                }
                //using (CodeSqlCe clsSqlCe = new CodeSqlCe())
                //{
                //    DataTable dtEmails = clsSqlCe.GetDataset("select [email] from [emaillist]");
                //    foreach (DataRow drEmail in dtEmails.Rows)
                //    {
                //        message.To.Add(drEmail["email"].ToString().Trim());
                //    }
                //    //message.CC.Add("Ryan.Hill@regions.com");
                //    //message.CC.Add("Wanda.Ibidapo@regions.com");
                //    //message.CC.Add("kelli.myrick@regions.com");
                //    //message.CC.Add("Eddie.Smith@regions.com");
                //}
                message.From = new System.Net.Mail.MailAddress("ablashaw@zaxbys.com", "CB Files");
                //sbMessage.Append("-----Rerun to test issue date modification-----");
                //sbMessage.Append(Environment.NewLine);
                //sbMessage.Append(Environment.NewLine);
                sbMessage.Append($"The BAI file {InFyle} has been processed to {OutFyle}.");
                sbMessage.Append(ExtMsg);
                message.Priority = MailPriority.High;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Body = sbMessage.ToString();
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.Send(message);

                smtp.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }

    }
}
