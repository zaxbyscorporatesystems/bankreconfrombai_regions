﻿using System;

namespace BankReconfromBAI_Regions
{
    class CodeGetTransactionDetails:IDisposable
    {

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        //properties
        public string Rec88 { set; get; }
        public string AcctUnit { set; get; }
        public string DistAcct { set; get; }
        public string SubAcct { set; get; }
        public string BaiCode { set; get; }

        //method
        public void UnScramble88()
        {
            DistAcct = "99999";
            SubAcct = string.Empty;
            try
            {

                switch (BaiCode)
                {
                    case "175":
                        AcctUnit = Rec88;
                        DistAcct = "13002";
                        SubAcct = "1355";
                        break;
                    case "399":
                        AcctUnit = Rec88;
                        DistAcct = "13002";
                        SubAcct = "1355";
                        break;
                    case "455":
                        AcctUnit = "900000";
                        DistAcct = "10099";
                        break;
                    case "631":
                        AcctUnit = Rec88;
                        DistAcct = "13002";
                        SubAcct = "1355";
                        break;                      
                    case "698":
                        AcctUnit = "900000";
                        DistAcct = "51700";     // per Donna 8/10/2016 "10099";
                        break;
                    case "699":
                        AcctUnit = Rec88;
                        DistAcct = "13002";    // per Donna 10/27/2016
                        SubAcct = "1355";      // per Donna 10/27/2016
                        break;
                    default:
                        break;
                }
                // clean up au
                if (AcctUnit.Trim().Length == 0)
                {
                    AcctUnit = "900000";
                    DistAcct = "99999";
                    SubAcct = string.Empty;
                }
                else
                {
                    string tmp = AcctUnit.Trim();
                    if (tmp.Length > 0)
                    {
                        using (CodeSQL clsSql = new CodeSQL())
                        {
                            switch (tmp.Length)
                            {
                                case 1:
                                    tmp = "0010" + tmp;
                                    tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                                    //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                                    break;
                                case 2:
                                    tmp = "001" + tmp;
                                    tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                                    //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                                    break;
                                case 3:
                                    tmp = "00" + tmp;
                                    tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                                    //tmp = clsSqlCe.ExecuteScalar($"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'");
                                    break;
                                case 4:
                                    tmp = $"{tmp}00";
                                    break;
                                case 5:
                                    tmp = clsSql.CmdScalar(Properties.Settings.Default.aConCMS, $"select [InforAU] from [Store_to_AU] where [StoreID]='{tmp}'").ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                        AcctUnit = tmp;
                    }
                }
            }
            catch (Exception)
            {
               // AcctUnit = tmp;
            }
        }

    }
}
